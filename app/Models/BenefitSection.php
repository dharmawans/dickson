<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BenefitSection extends Model
{
    protected $table = 'benefit_section';
    protected $fillable = ['icon', 'title', 'description'];
}
