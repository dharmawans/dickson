<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FreesampleSection extends Model
{
    protected $table = 'freesample_section';
    protected $fillable = ['heading', 'subheading', 'cover'];
}
